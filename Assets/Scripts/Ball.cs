﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public AudioSource hitAudio;
    public static float speed = 100;
    public static int score1 = 0;
    public static int score2 = 0;
    int racketCol = 0;
    int wallCol = 0;
        

    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;
    }

    float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight)
    {
        return (ballPos.y - racketPos.y) / racketHeight;
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "RacketLeft")
        {
            float y = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);
            
            Vector2 direction = new Vector2(1, y).normalized;
            GetComponent<Rigidbody2D>().velocity = direction * speed;

            wallCol = 0;
            racketCol = 1;

            hitAudio.Play();
        }
        
        if (col.gameObject.name == "RacketRight")
        {
            float y = hitFactor(transform.position,
                                col.transform.position,
                                col.collider.bounds.size.y);
            
            Vector2 direction = new Vector2(-1, y).normalized;
            GetComponent<Rigidbody2D>().velocity = direction * speed;

            wallCol = 0;
            racketCol = 2;

            hitAudio.Play();
        }

        if (col.gameObject.CompareTag("Wall"))
        {
            wallCol += 1;

            if (wallCol > 1)
                racketCol = 0;
            else
                return;
        }

        if (col.gameObject.CompareTag("Point"))
        {
            Destroy(col.gameObject);
            SpawnPoint.pointSpawned = 0;

            if (racketCol == 1)
                score1 += 1;
            else if (racketCol == 2)
                score2 += 1;
            else
                return;

            if (Random.Range(0, 10) == 0)
                Instantiate(this.gameObject, new Vector2(0, Random.Range(-34, 34)), Quaternion.identity);
        }
    }
}
