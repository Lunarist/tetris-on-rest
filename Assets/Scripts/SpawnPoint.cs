﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public GameObject point;
    public static int pointSpawned = 0;

    void Update()
    {
        if (pointSpawned == 0)
            SpawnerPoint();
        else
            return;
    }

    void SpawnerPoint()
    {
        Instantiate(point, new Vector2(Random.Range(-5, 5), Random.Range(-34, 34)), Quaternion.identity);
        pointSpawned = 1;
    }
}
