﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject ball;
    public Text txtScore1, txtScore2;

    void Start()
    {
        txtScore1.text = Ball.score1.ToString();
        txtScore2.text = Ball.score2.ToString();
        Instantiate(ball, new Vector2(0, Random.Range(-34, 34)), Quaternion.identity);
    }

    void Update()
    {
        if (Ball.score1 >= 3)
        {
            Ball.score1 = 3;
            Debug.Log("Player 1 is the Winner");
            Application.Quit();
        }
        else if (Ball.score2 >= 3)
        {
            Ball.score2 = 3;
            Debug.Log("Player 2 is the Winner");
            Application.Quit();
        }

        txtScore1.text = Ball.score1.ToString();
        txtScore2.text = Ball.score2.ToString();

    }
}
